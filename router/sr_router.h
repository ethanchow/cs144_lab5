/*-----------------------------------------------------------------------------
 * File: sr_router.h
 * Date: ?
 * Authors: Guido Apenzeller, Martin Casado, Virkam V.
 * Contact: casado@stanford.edu
 *
 *---------------------------------------------------------------------------*/

#ifndef SR_ROUTER_H
#define SR_ROUTER_H

#include <netinet/in.h>
#include <sys/time.h>
#include <stdio.h>

#include "sr_protocol.h"
#include "sr_arpcache.h"
#include "sr_if.h"
#include "sr_nat.h"


/* we dont like this debug , but what to do for varargs ? */
#ifdef _DEBUG_
#define Debug(x, args...) printf(x, ## args)
#define DebugMAC(x) \
  do { int ivyl; for(ivyl=0; ivyl<5; ivyl++) printf("%02x:", \
  (unsigned char)(x[ivyl])); printf("%02x",(unsigned char)(x[5])); } while (0)
#else
#define Debug(x, args...) do{}while(0)
#define DebugMAC(x) do{}while(0)
#endif

#define INIT_TTL 255
#define PACKET_DUMP_SIZE 1024
#define IP_SIZE     4
#define ETH_SIZE    6
  #define IP_HDR_LEN_BYTES 20

/* forward declare */
struct sr_if;
struct sr_rt;

/* ----------------------------------------------------------------------------
 * struct sr_instance
 *
 * Encapsulation of the state for a single virtual router.
 *
 * -------------------------------------------------------------------------- */

struct sr_instance
{
    int  sockfd;   /* socket to server */
    char user[32]; /* user name */
    char host[32]; /* host name */ 
    char template[30]; /* template name if any */
    unsigned short topo_id;
    struct sockaddr_in sr_addr; /* address to server */
    struct sr_if* if_list; /* list of interfaces */
    struct sr_rt* routing_table; /* routing table */
    struct sr_arpcache cache;   /* ARP cache */
    pthread_attr_t attr;
    struct sr_nat* nat; /**< Pointer to NAT state structure. */
    FILE* logfile;
};

/* -- sr_main.c -- */
int sr_verify_routing_table(struct sr_instance* sr);

/* -- sr_vns_comm.c -- */
int sr_send_packet(struct sr_instance* , uint8_t* , unsigned int , const char*);
int sr_connect_to_server(struct sr_instance* ,unsigned short , char* );
int sr_read_from_server(struct sr_instance* );

/* -- sr_router.c -- */
void sr_init(struct sr_instance* );
void sr_handlepacket(struct sr_instance* , uint8_t * , unsigned int , char* );

void send_arp_reply(struct sr_instance* sr,
        struct sr_if* iface,
        struct sr_ethernet_hdr *ethernet_hdr,
        struct sr_arp_hdr *arp_hdr);

/*
void write_eth_hdr(struct sr_ethernet_hdr *new_eth_hdr,
        uint16_t eth_type,
        uint8_t *ether_shost,
        uint8_t *ether_dhost);
*/
void handle_arp(struct sr_instance* sr,
        uint8_t * packet/* lent */,
        unsigned int len,
        char* interface/* lent */);

void handle_ip(struct sr_instance* sr,
        uint8_t * packet/* lent */,
        unsigned int len,
        char* interface/* lent */);

void handle_nat_ip(struct sr_instance* sr,
        uint8_t * packet/* lent */,
        unsigned int len,
        char* interface/* lent */);

int send_arp_request(struct sr_instance* sr, 
          struct sr_if* iface, unsigned char *dest_mac, uint32_t dest_ip, 
          unsigned char * src_mac);

void send_icmp_host_unreachable(struct sr_instance *sr, 
        uint8_t *packet,
        unsigned len,
        char *interface);
void add_ethernet_hdr(uint8_t *buffer, uint8_t *dhost, uint8_t *shost, uint16_t type);
void add_ip_hdr(uint8_t *buffer, uint16_t total_len, uint8_t ip_ttl,
        uint8_t ip_protocol, uint32_t ip_src, uint32_t ip_dst);
void add_icmp_hdr(uint8_t *buffer, uint8_t type, uint8_t code, int data_len);
void add_arp_hdr(
        uint8_t *buffer,
        unsigned short ar_op,
        unsigned char *ar_sha,
        unsigned char *ar_tha,
        uint32_t ar_sip,
        uint32_t ar_tip);
void add_icmp_t3_hdr(uint8_t *buffer, uint8_t type, uint8_t code, uint8_t* data);

void print_mac(unsigned char *mac_addr);
void print_ip(uint32_t ip_addr);
void print_ether_hdr(struct sr_ethernet_hdr *ethernet_hdr);
void print_arp_hdr(struct sr_arp_hdr *arp_hdr);
void print_interface(struct sr_if* interface);
int check_icmp_sum(struct sr_icmp_hdr *icmp_packet, int icmp_len);
struct sr_rt* longes_prefix_match (struct sr_instance *sr, uint32_t ip);
void longes_prefix_match_2 (struct sr_instance *sr, uint32_t ip, 
   unsigned long *gateway, char *interface);

void send_packets_in_arp_request(struct sr_instance* sr, 
  struct sr_arpreq * arp_req, 
  struct sr_arp_hdr * arp_hdr,
  char * interface);

void forward_ip_packet(struct sr_instance* sr,
        uint8_t * packet/* lent */,
        unsigned int len,
        uint8_t * src_mac, 
        uint8_t * dest_mac,
        char * interface);

void send_icmp_unreachables(
  struct sr_instance* sr,
  uint8_t * packet/* lent */,
  unsigned int len,
  uint8_t type,
  uint8_t code);


void send_icmp_echo_message(struct sr_instance* sr,
  uint8_t * packet/* lent */,
  unsigned int len,
  uint8_t type,
  uint8_t code);

/* -- sr_if.c -- */
void sr_add_interface(struct sr_instance* , const char* );
void sr_set_ether_ip(struct sr_instance* , uint32_t );
void sr_set_ether_addr(struct sr_instance* , const unsigned char* );
void sr_print_if_list(struct sr_instance* );



#endif /* SR_ROUTER_H */
