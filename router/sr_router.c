/**********************************************************************
 * file:  sr_router.c
 * date:  Mon Feb 18 12:50:42 PST 2002
 * Contact: casado@stanford.edu
 *
 * Description:
 *
 * This file contains all the functions that interact directly
 * with the routing table, as well as the main entry method
 * for routing.
 *
 **********************************************************************/

#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include "sr_if.h"
#include "sr_rt.h"
#include "sr_router.h"
#include "sr_protocol.h"
#include "sr_arpcache.h"
#include "sr_utils.h"
 #include "sr_nat.h"


/*---------------------------------------------------------------------
 * Method: sr_init(void)
 * Scope:  Global
 *
 * Initialize the routing subsystem
 *
 *---------------------------------------------------------------------*/

void sr_init(struct sr_instance* sr)
{
    /* REQUIRES */
    assert(sr);

    /* Initialize cache and cache cleanup thread */
    sr_arpcache_init(&(sr->cache));

    pthread_attr_init(&(sr->attr));
    pthread_attr_setdetachstate(&(sr->attr), PTHREAD_CREATE_JOINABLE);
    pthread_attr_setscope(&(sr->attr), PTHREAD_SCOPE_SYSTEM);
    pthread_attr_setscope(&(sr->attr), PTHREAD_SCOPE_SYSTEM);
    pthread_t thread;

    pthread_create(&thread, &(sr->attr), sr_arpcache_timeout, sr);
    
    /* Add initialization code here! */

} /* -- sr_init -- */

/*---------------------------------------------------------------------
 * Method: sr_handlepacket(uint8_t* p,char* interface)
 * Scope:  Global
 *
 * This method is called each time the router receives a packet on the
 * interface.  The packet buffer, the packet length and the receiving
 * interface are passed in as parameters. The packet is complete with
 * ethernet headers.
 *
 * Note: Both the packet buffer and the character's memory are handled
 * by sr_vns_comm.c that means do NOT delete either.  Make a copy of the
 * packet instead if you intend to keep it around beyond the scope of
 * the method call.
 *
 *---------------------------------------------------------------------*/

void sr_handlepacket(struct sr_instance* sr,
        uint8_t * packet/* lent */,
        unsigned int len,
        char* interface/* lent */)
{
  /* REQUIRES */
  assert(sr);
  assert(packet);
  assert(interface);k

  printf("\n=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=\n*** -> Received packet of length %d \n",len);

if (len < sizeof(struct sr_ethernet_hdr))
    return
  print_hdr_eth(packet);

  if (ethertype(packet) == ethertype_arp)
  {
    printf("INFO:    Received packet is ARP\n");
    handle_arp(sr, packet,len,interface);
  }
  else if(ethertype(packet) == ethertype_ip)
  {
      printf("INFO:    Received packet is etherIP\n");
      handle_ip(sr, packet, len, interface);
  }
  else
  {
    return;
  }
  
}/* end of sr_handlepacket */

void handle_nat_ip(struct sr_instance* sr,
        uint8_t * packet/* lent */,
        unsigned int len,
        char* interface/* lent */); {







        }

void send_icmp_echo_message(struct sr_instance* sr,
  uint8_t * packet/* lent */,
  unsigned int len,
  uint8_t type,
  uint8_t code){

  /*struct sr_ethernet_hdr * old_eth_hdr;*/
  struct sr_ethernet_hdr * new_eth_hdr;
  struct sr_ip_hdr * old_ip_hdr;
  struct sr_ip_hdr * new_ip_hdr;
  struct sr_icmp_hdr * icmp_hdr;
  struct sr_rt* best_rt_entry;
  struct sr_if* out_iface;
  struct sr_arpentry * arp_entry;

  /*old_eth_hdr = (struct sr_ethernet_hdr *) packet;*/
  old_ip_hdr = (struct sr_ip_hdr * ) (packet + sizeof(struct sr_ethernet_hdr));

  printf("Sending icmp echo: Type%d Code%d\n", type, code);

  /*If this is a echo reply message*/
  if (type == 0 && code == 0){
    printf("INFO:   Sending ICMP Echo Reply");

    uint16_t total_ip_len = ntohs(old_ip_hdr->ip_len);
    unsigned int total_len = sizeof(struct sr_ethernet_hdr) + total_ip_len;

    uint8_t *buffer = (uint8_t *) malloc(total_len);

    new_eth_hdr = (struct sr_ethernet_hdr *) buffer;
    new_ip_hdr = (struct sr_ip_hdr * ) (buffer + sizeof(struct sr_ethernet_hdr));

    icmp_hdr = (struct sr_icmp_hdr *) (buffer+ sizeof(struct sr_ethernet_hdr) + sizeof(struct sr_ip_hdr));

    /*Look up the routing entry for the source ip of the packet*/
    best_rt_entry = longes_prefix_match(sr, old_ip_hdr->ip_src);
    if (best_rt_entry == NULL){
      printf("INFO:   Source ip not found. No ICMP error msg sent.\n");
      return;
    }

    /*get the interface from where the icmp will be sent out*/
    out_iface = sr_get_interface(sr, best_rt_entry->interface);

    printf("INFO:   ARP entry found. Forwarding the packet now\n");
    /*forward_ip_packet(sr, packet, len, out_iface->addr, arp_entry->mac, out_iface->name);*/
    /*This copies everything including icmp header*/
    memcpy((uint8_t *) new_ip_hdr, old_ip_hdr, total_ip_len);

    /*Swap the source and dest ip in the new ip hdr*/

    /*this will be the dest mac in the ethernet header*/
    new_ip_hdr->ip_src= old_ip_hdr->ip_dst;
    new_ip_hdr->ip_dst= old_ip_hdr->ip_src;

    uint16_t icmp_length = total_ip_len - sizeof(struct sr_ip_hdr);

    add_icmp_hdr((uint8_t *)icmp_hdr, type, code, icmp_length);

    memset(new_eth_hdr->ether_dhost, 0x00, ETHER_ADDR_LEN);
    memcpy(new_eth_hdr->ether_shost, out_iface->addr, ETHER_ADDR_LEN);
    new_eth_hdr->ether_type = htons(ethertype_ip);

    /*this will be the dest mac in the ethernet header*/
    arp_entry = sr_arpcache_lookup(&sr->cache, old_ip_hdr->ip_src);
    if(arp_entry){

     /* add_ethernet_hdr((uint8_t *)new_eth_hdr, arp_entry->mac, out_iface->addr, ethertype_ip);*/

      memcpy(new_eth_hdr->ether_dhost, arp_entry->mac, ETHER_ADDR_LEN);
      sr_send_packet(sr, buffer, total_len, out_iface->name);

      free(buffer);
    }
    else{
      printf("INFO:  ARP entry not found. Queue the packet in the ARP cache\n");
      struct sr_arpreq *req;
      req = sr_arpcache_queuereq(&sr->cache, old_ip_hdr->ip_src,
            buffer,total_len, out_iface->name);
      handle_arpreq(sr, &sr->cache, req);
      
    }

    

  }
}

/*this function can cover all unreachables such as "port" and "net" and as well as TTL timeout.*/
void send_icmp_unreachables(
  struct sr_instance* sr,
  uint8_t * packet/* lent */,
  unsigned int len,
  uint8_t type,
  uint8_t code){

  struct sr_ethernet_hdr * old_eth_hdr;
  struct sr_ethernet_hdr * new_eth_hdr;
  struct sr_ip_hdr * old_ip_hdr;
  struct sr_ip_hdr * new_ip_hdr;
  struct sr_icmp_t3_hdr * icmp_t3_hdr;
  struct sr_rt* best_rt_entry;
  struct sr_if* out_iface;

  old_eth_hdr = (struct sr_ethernet_hdr *) packet;
  old_ip_hdr = (struct sr_ip_hdr * ) (packet + sizeof(struct sr_ethernet_hdr));

  
  /*Compute the total len of icmp message*/
  unsigned int total_len = sizeof(struct sr_ethernet_hdr) 
    + sizeof(struct sr_ip_hdr) 
    + sizeof(struct sr_icmp_t3_hdr);

  uint8_t *buffer = (uint8_t *) malloc(total_len);

  new_eth_hdr = (struct sr_ethernet_hdr *) buffer;
  new_ip_hdr = (struct sr_ip_hdr * ) (buffer + sizeof(struct sr_ethernet_hdr));
  icmp_t3_hdr = (struct sr_icmp_t3_hdr *) (buffer + sizeof(struct sr_ethernet_hdr) + sizeof(struct sr_ip_hdr));

  /*Look up the routing entry for the source ip of the packet*/
  best_rt_entry = longes_prefix_match(sr, old_ip_hdr->ip_src);
  if (best_rt_entry == NULL){
    printf("INFO:   Source ip not found. No ICMP error msg sent.");
    free(buffer);
    return;
  }

  /*get the interface from where the icmp will be sent out*/
  out_iface = sr_get_interface(sr, best_rt_entry->interface);

  uint16_t total_ip_len = sizeof(struct sr_ip_hdr) + sizeof(struct sr_icmp_t3_hdr);

  add_ip_hdr((uint8_t *)new_ip_hdr, htons(total_ip_len), INIT_TTL, ip_protocol_icmp, out_iface->ip, old_ip_hdr->ip_src);

  uint8_t * ip_hdr_data = malloc(ICMP_DATA_SIZE);
  memcpy (ip_hdr_data, (uint8_t *)old_ip_hdr, ICMP_DATA_SIZE);
  add_icmp_t3_hdr((uint8_t *)icmp_t3_hdr, type, code, ip_hdr_data);

  free(ip_hdr_data);

  /*use the source mac address in the old ethernet head as the dest mac addr for the icmp packet*/
  add_ethernet_hdr((uint8_t *)new_eth_hdr, old_eth_hdr->ether_shost, out_iface->addr, ethertype_ip);
  sr_send_packet(sr, buffer, total_len, out_iface->name);

  free(buffer);
  
}

void forward_ip_packet(struct sr_instance* sr,
        uint8_t * packet/* lent */,
        unsigned int len,
        uint8_t * src_mac, 
        uint8_t * dest_mac,
        char * interface){
  printf("Forwarding packet\n\n");
  printf("Interface: %s\n", interface);

  struct sr_ip_hdr * ip_hdr = 0;
  struct sr_ethernet_hdr * ether_hdr = 0;


  uint8_t * buffer = malloc(len);

  memcpy(buffer, packet, len);

  ether_hdr = (struct sr_ethernet_hdr *) buffer;
  ip_hdr = (struct sr_ip_hdr *) (buffer + sizeof(struct sr_ethernet_hdr));

  /*decrement the ttl*/
  ip_hdr->ip_ttl--;

  if(ip_hdr->ip_ttl == 0){
    printf("TTL is zero. drop the packet");
    free(buffer);
    return;
  }

  /*Update the checksum */
  ip_hdr->ip_sum = 0;
  ip_hdr->ip_sum = cksum(ip_hdr, (ip_hdr->ip_hl * 4));

  add_ethernet_hdr(buffer, dest_mac, src_mac, ethertype_ip);

  printf("INFO:   Forward Ethernet Header:");
  print_ether_hdr(ether_hdr);
  printf("INFO:   Forward IP Header");
  print_hdr_ip((uint8_t *) ip_hdr);

  sr_send_packet(sr, buffer, len, interface);
  free(buffer);

}

void add_ethernet_hdr(uint8_t *buffer, uint8_t *dhost, uint8_t *shost, uint16_t type){

  struct sr_ethernet_hdr *ethernet_hdr = NULL;
  ethernet_hdr = (struct sr_ethernet_hdr *) buffer;
  memcpy(ethernet_hdr->ether_dhost, dhost, ETHER_ADDR_LEN);
  memcpy(ethernet_hdr->ether_shost, shost, ETHER_ADDR_LEN);
  ethernet_hdr->ether_type = htons(type);
  printf("Printing new ethernet header...\n");
  print_ether_hdr(ethernet_hdr);
}

void add_ip_hdr(uint8_t *buffer, uint16_t total_len, uint8_t ip_ttl,
        uint8_t ip_protocol, uint32_t ip_src, uint32_t ip_dst){

  struct sr_ip_hdr *ip_hdr = NULL;
  ip_hdr = (struct sr_ip_hdr *) buffer;
  ip_hdr->ip_hl = 0x05;
  ip_hdr->ip_v = 0x04;
  ip_hdr->ip_tos = 0x00;
  ip_hdr->ip_len = total_len;
  ip_hdr->ip_id = 65535;
  ip_hdr->ip_off = 0x00;
  ip_hdr->ip_ttl = ip_ttl;
  ip_hdr->ip_p = ip_protocol;
  ip_hdr->ip_sum = 0x0000;
  ip_hdr->ip_src = ip_src;
  ip_hdr->ip_dst = ip_dst;
  ip_hdr->ip_sum = cksum(ip_hdr, ip_hdr->ip_hl * 4);
  printf("Printing new IP header...\n");
  print_hdr_ip((uint8_t *) ip_hdr);
}


void add_icmp_hdr(uint8_t *buffer, uint8_t type, uint8_t code, int data_len) {
  struct sr_icmp_hdr *icmp_hdr = NULL;
  icmp_hdr = (struct sr_icmp_hdr *) buffer;
  icmp_hdr->icmp_type = type;
  icmp_hdr->icmp_code = code;
  icmp_hdr->icmp_sum = 0x0000;
  icmp_hdr->icmp_sum = cksum(icmp_hdr, data_len);
  printf("Printing new ICMP header...\n");
  print_hdr_icmp((uint8_t *) icmp_hdr);
}


void add_icmp_t3_hdr(uint8_t *buffer, uint8_t type, uint8_t code, uint8_t* data){
  struct sr_icmp_t3_hdr *icmp_t3_hdr = NULL;
  icmp_t3_hdr = (struct sr_icmp_t3_hdr *) buffer;
  icmp_t3_hdr->icmp_type = type;
  icmp_t3_hdr->icmp_code = code;

  memcpy(icmp_t3_hdr->data, data, ICMP_DATA_SIZE);

  icmp_t3_hdr->icmp_sum = 0;
  uint16_t icmp_sum = cksum(icmp_t3_hdr, sizeof(sr_icmp_t3_hdr_t));
  icmp_t3_hdr->icmp_sum = icmp_sum;

  printf("Printing new Type 3 ICMP header...\n");
  print_hdr_icmp((uint8_t *) icmp_t3_hdr);
}

/*
void write_eth_hdr(struct sr_ethernet_hdr *new_eth_hdr,
        uint16_t eth_type,
        uint8_t *ether_shost,
        uint8_t *ether_dhost){
  new_eth_hdr->ether_type = eth_type;
  memcpy(new_eth_hdr->ether_dhost, ether_dhost, ETHER_ADDR_LEN);
  memcpy(new_eth_hdr->ether_shost, ether_shost, ETHER_ADDR_LEN);
}
*/
void add_arp_hdr(
        uint8_t * buffer,
        unsigned short ar_op,
        unsigned char *ar_sha,
        unsigned char *ar_tha,
        uint32_t ar_sip,
        uint32_t ar_tip){

  struct sr_arp_hdr * new_arp_hdr;

  new_arp_hdr = (struct sr_arp_hdr*) buffer;
  /* fill in the arp header info */
  new_arp_hdr->ar_hrd = htons(arp_hrd_ethernet);
  new_arp_hdr->ar_pro = htons(arp_pro_ip4);
  new_arp_hdr->ar_hln = ETHER_ADDR_LEN;
  new_arp_hdr->ar_pln = sizeof(uint32_t);
  new_arp_hdr->ar_op = htons(ar_op);

  /* Edit the ARP target mac address to the router mac address */
  memcpy(new_arp_hdr->ar_sha, ar_sha, ETHER_ADDR_LEN);
  new_arp_hdr->ar_sip = ar_sip;
  memcpy(new_arp_hdr->ar_tha, ar_tha, ETHER_ADDR_LEN);
  new_arp_hdr->ar_tip = ar_tip;
}

void handle_ip(struct sr_instance* sr,
        uint8_t * packet/* lent */,
        unsigned int len,
        char* interface/* lent */){

    /* Cast the raw ethernet frame */  
    /* -------------------------------- */
    /* Cast the IP header               */
    /* -------------------------------- */
    struct sr_ip_hdr *ip_hdr = (struct sr_ip_hdr *) (packet + sizeof(struct sr_ethernet_hdr));

    /* Print the IP header */
    print_hdr_ip((uint8_t *) ip_hdr);
    
    /* --------------------------------- */
    /* To make a copy of the IP header   */
    /* and make the checksum part 0X0000 */
    /* --------------------------------- */
    struct sr_ip_hdr *ip_hdr_cpy = NULL;
    ip_hdr_cpy = malloc( sizeof(struct sr_ip_hdr));
    memcpy(ip_hdr_cpy, ip_hdr, sizeof(struct sr_ip_hdr));
    ip_hdr_cpy->ip_sum = 0x0000;

    /* --------------------------------- */
    /* If the Checksum NOT matching      */
    /* Discard the packet                */
    /* --------------------------------- */
    if (ip_hdr->ip_sum != cksum(ip_hdr_cpy, ip_hdr->ip_hl * 4)){
      printf("    IP Checksum NOT Matching\n");
      free(ip_hdr_cpy);
      return;
    }
    
    free(ip_hdr_cpy);

    /*check size of packet large enough to contain ip header and ethernet_hdr*/
    if (len < sizeof(struct sr_ip_hdr) + sizeof(struct sr_ethernet_hdr))
      return;
    }

    if(sr->nat != NULL) /*from now on the router works as a simple NAT*/
    {
      handle_nat_ip(sr, packet, len, interface);
    }
    else /* from now on the rest of the router works as a simple router*/
    {

    /* Get the interface */
    struct sr_if* iface = sr->iface
    while(iface)
    {
        if(ip_hdr->ip_dst == iface->ip) break;    
        iface=iface->next;
    }
    /* --------------------------------- */
    /* If the IP packet is for me        */
    /* --------------------------------- */
    if (iface){

      printf("INFO:   IP Packet is FOR ME)\n");

      /* --------------------------------- */
      /* If it is ICMP packet              */
      /* --------------------------------- */
      if (ip_hdr->ip_p == ip_protocol_icmp) {

        printf("*** ->It is ICMP packet (FOR ME)\n");

        /* Cast the ICMP packet*/
        struct sr_icmp_hdr *icmp_hdr;
        icmp_hdr = (struct sr_icmp_hdr *) (packet + sizeof(struct sr_ethernet_hdr) + (ip_hdr->ip_hl * 4));
        int icmp_len = 0;
        icmp_len = ntohs(ip_hdr->ip_len) - (ip_hdr->ip_hl * 4);

        /* --------------------------------- */
        /* If the ICMP checksum NOT MACTHING */
        /* Discard the packet                */
        /* --------------------------------- */
        if (check_icmp_sum(icmp_hdr, icmp_len)){
          printf("INFO:   ICMP checksum matched. Send reply\n");
          send_icmp_echo_message(sr, packet, len, 0, 0);
        }else{
          printf("INFO:   ICMP checksum not matched. Drop\n");
        }
        /* --------------------------------- */
        /* Check the ICMP type               */
        /* --------------------------------- */
        
      }

      /* --------------------------------- */
      /* If it is TCP/UDP packet           */
      /* --------------------------------- */
      else if (ip_hdr->ip_p == ip_protocol_tcp|| ip_hdr->ip_p == ip_protocol_udp){
        printf("INFO:   TCP/UDP packet received for me. Sending icmp port unreachable.\n");
        send_icmp_unreachables(sr, packet, len, 3, 3);
      }
    }

    /* --------------------------------- */
    /* If the IP packet is NOT for me    */
    /* --------------------------------- */
    else{
      printf("*** ->IP packet (NOT FOR ME)\n");

      /* --------------------------------- */
      /* Check the routing table           */
      /* Perform LPM                       */
      /* --------------------------------- */
      sr_print_routing_table(sr);   /* Print the Routing Table for debug */

      struct sr_rt* best_rt_entry;
      best_rt_entry = longes_prefix_match(sr, ip_hdr->ip_dst);


      /* --------------------------------- */
      /* If it is ICMP packet              */
      /* --------------------------------- */
      
      if(best_rt_entry != 0){
        printf("INFO:   Best matching entry found \n");

        /*the outgoing interface, whose mac address will be used as the source mac addr in ethernet hdr*/
        struct sr_if * out_iface = 0;
        struct sr_arpentry *arp_entry = 0;

        out_iface = sr_get_interface(sr, best_rt_entry->interface);

        /*---------------------------------*/
        /* Check ARP cache                  */
        /*---------------------------------*/
        
        /*this will be the dest mac in the ethernet header*/
        arp_entry = sr_arpcache_lookup(&sr->cache, ip_hdr->ip_dst);
        if(arp_entry){
          printf("INFO:   ARP entry found. Forwarding the packet now\n");
          forward_ip_packet(sr, packet, len, out_iface->addr, arp_entry->mac, out_iface->name);
        }
        else{
          printf("INFO:  ARP entry not found. Queue the packet in the ARP cache\n");
          struct sr_arpreq *req;
          req = sr_arpcache_queuereq(&sr->cache, ip_hdr->ip_dst,
                packet,len, out_iface->name);
          handle_arpreq(sr, &sr->cache, req);
          
        }
      }else{
         /* --------------------------------- */
        /* If the matching ip not found      */
        /* Send ICMP net unreachable         */
        /* --------------------------------- */
        
        printf("INFO:   No matching interface found. Sending icmp net unreachable.\n");
        send_icmp_unreachables(sr, packet, len, 3, 0);
      }  
    }
    }
  }
}


void handle_arp(struct sr_instance* sr,
        uint8_t * packet/* lent */,
        unsigned int len,
        char* interface/* lent */){

  
  struct sr_ethernet_hdr *ethernet_hdr;
  struct sr_arp_hdr *arp_hdr;
  struct sr_if* iface = 0;

  /* Cast the raw ethernet frame */
  ethernet_hdr = (struct sr_ethernet_hdr *)packet;

  /* Get the interface */
  iface = sr_get_interface(sr, interface);

  /* Cast the ARP header */
  arp_hdr = (struct sr_arp_hdr *) (packet + sizeof(struct sr_ethernet_hdr));

  /* -------------------------------- */
  /* It's a arp send_arp_request      */
  /* -------------------------------- */
  if (arp_hdr->ar_op == htons(arp_op_request)){
    printf("INFO:    ARP request message received\n");
  /* -------------------------------- */
  /* If ARP request is to me          */
  /* -------------------------------- */
    if(arp_hdr->ar_tip == iface->ip){
    /* -------------------------------- */
    /* Construct an ARP reply and send  */
    /* -------------------------------- */
      printf("INFO:    APR request is to me. Reply Now.\n");
      printf("Printing new arp header...\n");
      print_arp_hdr(arp_hdr);

      /* Get the new len of the new packet and malloc memory space for it */
      unsigned int new_len = sizeof(sr_ethernet_hdr_t) + sizeof(struct sr_arp_hdr);
      uint8_t * buffer = (uint8_t *) malloc(new_len);

      /* Cast the new ethernet frame and ARP header */
      struct sr_ethernet_hdr * new_ethernet_hdr = 0;
      new_ethernet_hdr = (struct sr_ethernet_hdr *) buffer;
      struct sr_arp_hdr * new_arp_hdr =  0;
      new_arp_hdr = (struct sr_arp_hdr *) (buffer + sizeof(struct sr_ethernet_hdr));

      /*write_eth_hdr(new_ethernet_hdr, ethernet_hdr->ether_type, iface->addr, ethernet_hdr->ether_shost);*/
      add_ethernet_hdr((uint8_t *) new_ethernet_hdr,ethernet_hdr->ether_shost, iface->addr, ntohs(ethernet_hdr->ether_type));

      add_arp_hdr((uint8_t *)new_arp_hdr, arp_op_reply, iface->addr, arp_hdr->ar_sha, iface->ip, arp_hdr->ar_sip);
      /* Send the packet and free the space */
      sr_send_packet(sr, buffer, new_len, iface->name);
      free(buffer);
    }
  /* -------------------------------- */
  /* If ARP request is NOT to me          */
  /* -------------------------------- */
    else{
      printf("INFO:    ARP request is Not to me.\n");
    }

    
  /* -------------------------------- */
  /* The ARP packet reply to me       */
  /* -------------------------------- */
  }
  else if(arp_hdr->ar_op == htons(arp_op_reply)){
    printf("INFO:    ARP reply message received\n");
    print_arp_hdr(arp_hdr);
    /* -------------------------------- */
    /* Go through my request queue      */
    /* -------------------------------- */

    struct sr_arpreq * arp_req = NULL;
    /* Use sr_arpcache_insert to find the request that look for the ip->mac mapping"*/
    /* The function will also add an entry for this ip->mac mapping*/
    arp_req = (struct sr_arpreq *) sr_arpcache_insert(&sr->cache, arp_hdr->ar_sha, arp_hdr->ar_sip);

    /*  If request if found, send all the packets in the arp requests*/
    if (arp_req != NULL){
      send_packets_in_arp_request(sr, arp_req, arp_hdr, interface);
      /*destroy the arp request after send all the packets in the request*/
      sr_arpreq_destroy(&sr->cache, arp_req);
    }
      /* -------------------------------- */
      /* Send outstanding packets         */
      /* -------------------------------- */
  }
}

/**
* Send all the packets associated with the request
**/
void send_packets_in_arp_request(struct sr_instance* sr, 
  struct sr_arpreq * arp_req, 
  struct sr_arp_hdr * arp_hdr,
  char * interface){

  struct sr_ethernet_hdr * ethernet_hdr;
  struct sr_if * iface;

  iface = (struct sr_if *) sr_get_interface(sr, interface);


  struct sr_packet* packet = NULL;
  packet = (struct sr_packet*) arp_req->packets;


  while (packet!= NULL){
    unsigned int length = packet->len;
    uint8_t * buffer = malloc(length);

    memcpy(buffer, packet->buf, length);

    ethernet_hdr = (struct sr_ethernet_hdr *) buffer;
    memcpy(ethernet_hdr->ether_dhost, arp_hdr->ar_sha, ETHER_ADDR_LEN);
    memcpy(ethernet_hdr->ether_shost, iface->addr, ETHER_ADDR_LEN);

    /* Send the packet and free the space */
    sr_send_packet(sr, buffer, length, interface);
    packet = packet->next;

    free(buffer);
  }
}


void send_arp_reply(struct sr_instance* sr,
        struct sr_if* iface,
        struct sr_ethernet_hdr *ethernet_hdr,
        struct sr_arp_hdr *arp_hdr)
{
  struct sr_ethernet_hdr * new_ethernet_hdr = 0;
  struct sr_arp_hdr * new_arp_hdr =  0;

  printf("Sending ARP reply...\n");

  /* Get the new len of the new packet and malloc memory space for it */
  unsigned int new_len = sizeof(sr_ethernet_hdr_t) + sizeof(struct sr_arp_hdr);
  uint8_t * buffer = (uint8_t *) malloc(new_len);

  /* Cast the new ethernet frame and ARP header */
  
  new_ethernet_hdr = (struct sr_ethernet_hdr *) buffer;
  new_arp_hdr = (struct sr_arp_hdr *) (buffer + sizeof(struct sr_ethernet_hdr));

  /*write_eth_hdr(new_ethernet_hdr, ethernet_hdr->ether_type, iface->addr, ethernet_hdr->ether_shost);*/

  add_ethernet_hdr((uint8_t *)new_ethernet_hdr, ethernet_hdr->ether_shost, iface->addr, ntohs(ethernet_hdr->ether_type));

  add_arp_hdr((uint8_t *)new_arp_hdr, arp_op_reply, iface->addr, arp_hdr->ar_sha, iface->ip, arp_hdr->ar_sip);

  printf("Printing new ethernet header...\n");
  print_ether_hdr(new_ethernet_hdr);
  printf("Printing new arp header...\n");
  print_arp_hdr(new_arp_hdr);

  /* Send the packet and free the space */
  sr_send_packet(sr, buffer, new_len, iface->name);
  free(buffer);
}


void print_mac(unsigned char *mac_addr){
  #if __BYTE_ORDER == __LITTLE_ENDIAN
  printf("%02x:%02x:%02x:%02x:%02x:%02x\n",
  (unsigned char) mac_addr[0],
  (unsigned char) mac_addr[1],
  (unsigned char) mac_addr[2],
  (unsigned char) mac_addr[3],
  (unsigned char) mac_addr[4],
  (unsigned char) mac_addr[5]);
  #elif __BYTE_ORDER == __BIG_ENDIAN
  printf("%02x:%02x:%02x:%02x:%02x:%02x\n",
  (unsigned char) mac_addr[5],
  (unsigned char) mac_addr[4],
  (unsigned char) mac_addr[3],
  (unsigned char) mac_addr[2],
  (unsigned char) mac_addr[1],
  (unsigned char) mac_addr[0]);
  #else
  #error "Byte ordering ot specified " 
  #endif 
}

void print_ether_hdr(struct sr_ethernet_hdr *ethernet_hdr){
  
  printf("***->Ethernet Header\n");
  printf("     Destination Address: ");
  print_mac(ethernet_hdr->ether_dhost);
  printf("     Source Address: ");
  print_mac(ethernet_hdr->ether_shost);
}

void print_arp_hdr(struct sr_arp_hdr *arp_hdr){


  printf("***->ARP Header\n");
  printf("    Hardware type: 0x%04x\n", arp_hdr->ar_hrd);
  printf("    Protocol type: 0x%04x\n", arp_hdr->ar_pro);
  printf("    Hardware Length: %o\n", arp_hdr->ar_hln);
  printf("    Protocal Length: %o\n", arp_hdr->ar_pln);

  printf("    Sender IP: ");
  print_ip(arp_hdr->ar_sip);
  printf("    Sender MAC: ");
  print_mac(arp_hdr->ar_sha);
  printf("    Target IP: ");
  print_ip(arp_hdr->ar_tip);
  printf("    Target MAC: ");
  print_mac(arp_hdr->ar_tha);
}

void print_interface(struct sr_if* interface_st){

  printf("***-> Interface\n");
  printf("   name: %s\n", interface_st->name);
  printf("   addr: ");
  print_mac(interface_st->addr);
  printf("   ip: ");
  print_ip(interface_st->ip);
  printf("   speed: %d\n", interface_st->speed);
}

void print_ip(uint32_t ip_addr){
  unsigned char bytes[4];
  bytes[0] = ip_addr & 0xFF;
  bytes[1] = (ip_addr >> 8) & 0xFF;
  bytes[2] = (ip_addr >> 16) & 0xFF;
  bytes[3] = (ip_addr >> 24) & 0xFF; 
  #if __BYTE_ORDER == __LITTLE_ENDIAN
  printf("%d.%d.%d.%d\n", bytes[0], bytes[1], bytes[2], bytes[3]);
  #elif __BYTE_ORDER == __BIG_ENDIAN
  printf("%d.%d.%d.%d\n", bytes[3], bytes[2], bytes[1], bytes[0]);
  #else
  #error "Byte ordering ot specified " 
  #endif 
}

int check_icmp_sum(struct sr_icmp_hdr *icmp_packet, int icmp_len){


  /* Check the Checksum */
  struct sr_icmp_hdr *ip_icmp_cpy = NULL;
  ip_icmp_cpy = malloc(icmp_len);
  memcpy(ip_icmp_cpy, icmp_packet, icmp_len);
  ip_icmp_cpy->icmp_sum = 0x0000;


  if (icmp_packet->icmp_sum != cksum(ip_icmp_cpy, icmp_len)){
    printf("ICMP checksum NOT MATCHING\n");
    free(ip_icmp_cpy);
    return 0;
  }
  else{
    printf("ICMP checksum MATCHING\n");
    free(ip_icmp_cpy);
    return 1;
    
  }
}

int send_arp_request(struct sr_instance* sr, 
          struct sr_if* iface, unsigned char *dest_mac, uint32_t dest_ip, 
          unsigned char * src_mac){ 

  struct sr_ethernet_hdr * new_ethernet_hdr = 0;
  struct sr_arp_hdr * new_arp_hdr =  0;
  unsigned char *arp_dest_mac = malloc(ETHER_ADDR_LEN);
  memset (arp_dest_mac, 0x00, ETHER_ADDR_LEN);
  memset (dest_mac, 0xff, ETHER_ADDR_LEN);

 
  /* Get the new len of the new packet and malloc memory space for it */
  unsigned int new_len = sizeof(sr_ethernet_hdr_t) + sizeof(struct sr_arp_hdr);
  uint8_t * buffer = (uint8_t *) malloc(new_len);

  /* Cast the new ethernet frame and ARP header */
  new_ethernet_hdr = (struct sr_ethernet_hdr *) buffer;
  new_arp_hdr = (struct sr_arp_hdr *) (buffer + sizeof(struct sr_ethernet_hdr));


  /*write_eth_hdr(new_ethernet_hdr, ntohs(ethertype_arp), iface->addr, dest_mac);*/

  add_ethernet_hdr((uint8_t *)new_ethernet_hdr, dest_mac, iface->addr,ethertype_arp);
  add_arp_hdr((uint8_t *)new_arp_hdr, arp_op_request,
    src_mac, arp_dest_mac, iface->ip, dest_ip);

  free(arp_dest_mac);

  printf("Printing new ethernet header...\n");
  print_ether_hdr(new_ethernet_hdr);
  printf("Printing new arp header...\n");
  print_arp_hdr(new_arp_hdr);

  /* Send the packet and free the space */
  sr_send_packet(sr, buffer, new_len, iface->name);
  free(buffer);
  return 0;


}


struct sr_rt* longes_prefix_match (struct sr_instance *sr, uint32_t ip) {

  assert (sr->routing_table); 
  
  struct sr_rt *current = 0;
  struct sr_rt *best_routing_entry = 0;

  current = sr->routing_table;
  


  uint32_t best_mask = 0;
  
  unsigned long prefix_mask, ip_mask;

  while(current != NULL) {
    prefix_mask = current->dest.s_addr & current->mask.s_addr;
    ip_mask = ip & current->mask.s_addr;

    if (prefix_mask == ip_mask && (current->mask.s_addr >= best_mask)) {
      
      best_routing_entry = current;
    }
    current = current->next;
  }
  
  return best_routing_entry;
}



void longes_prefix_match_2 (struct sr_instance *sr, uint32_t ip, 
   unsigned long *gateway, char *interface) {

 assert (sr->routing_table); 
  
  struct sr_rt *current = sr->routing_table;
  uint32_t gw = 0;
  uint32_t best_mask = 0;
  char *best_interface;
  unsigned long prefix_mask, ip_mask;
  int is_head = 1;

  while(current != NULL) {
    prefix_mask = current->dest.s_addr & current->mask.s_addr;
    ip_mask = ip & current->mask.s_addr;
    if (prefix_mask == ip_mask && (current->mask.s_addr >= best_mask || is_head)) {
      gw = current->gw.s_addr;
      best_mask = current->mask.s_addr;
      best_interface = (char *)current->interface;
      is_head = 0;
    }
    current = current->next;
  }
  strcpy (interface, best_interface);
  *gateway = gw;
}







/* end sr_ForwardPacket */